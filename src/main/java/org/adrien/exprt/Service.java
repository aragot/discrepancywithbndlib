package org.adrien.exprt;

public interface Service
{
    void launch();
}
